const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const timestamp = require('mongoose-timestamp');

const Schema = mongoose.Schema;
const UserSchema = new Schema({
  local: {
    email: String,
    password: String,
  },
});

UserSchema.plugin(timestamp);

UserSchema.methods.generateHash = function generateHash(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

UserSchema.methods.validPassword = function validPassword(password) {
  return bcrypt.compareSync(password, this.local.password);
};

module.exports = mongoose.model('User', UserSchema);
