require('dotenv').config({ silent: true });
const http = require('http');
const config = require('./config');

const app = require('./app');

const server = http.createServer(app);
function onListen(err) {
  if (err) {
    console.error(err, 'error starting server');
    process.exit(1);
  }
  const address = server.address();
  console.log("listening on http://" + address.address + ":" + address.port);
}

server.once('error', (err) => {
  if (err.code === 'EADDRINUSE') { return onListen(err); }
  throw err;
});

if (config.HOST === '0.0.0.0' || config.HOST === '::') {
  server.listen(config.PORT, onListen);
} else {
  server.listen(config.PORT, config.HOST, onListen);
}
