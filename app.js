const express = require('express');
// const favicon = require('serve-favicon');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const passport = require('passport');
const flash = require('connect-flash');
const morgan = require('morgan');
const session = require('express-session');


require('./config/database');
const app = express();

app.use(morgan('dev'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.set('view engine', 'ejs');
app.set('trust proxy', ['loopback', 'linklocal', 'uniquelocal']);

app.use(require('serve-static')(`${__dirname}/public`));

app.use(session({ secret: 'savetoenvconfig' })); // session secret
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

require('./routes.js')(app, passport);
require('./config/passport')(passport);

// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

module.exports = app;
