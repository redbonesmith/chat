const config = module.exports;

config.HOST = process.env.HOST || '127.0.0.1';
config.PORT = parseInt(process.env.PORT || '3000', 10);

config.MONGODB_URI = process.env.MONGODB_URI || 'mongodb://localhost/chat';
