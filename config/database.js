const mongoose = require('mongoose');
const config = require('../config');
mongoose.Promise = require('bluebird');

mongoose.set('debug', config.MONGOOSE_DEBUG);
mongoose.connect(config.MONGODB_URI);
