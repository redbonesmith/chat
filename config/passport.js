const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/user');

const strategySettings = {
  usernameField: 'email',
  passwordField: 'password',
  passReqToCallback: true,
};

function strategyCbSignup(req, email, password, done) {
  process.nextTick(() => {
    User.findOne({ 'local.email': email }, (err, user) => {
      if (err) { return done(err); }
      if (user) {
        done(null, false, req.flash('signupMessage'
        , 'That email is already taken.'));
      } else {
        const newUser = new User();
        newUser.local.email = email;
        newUser.local.password = newUser.generateHash(password);
        newUser.save((_err) => {
          if (_err) { throw _err; }
          return done(null, newUser);
        });
      }

      return done(null);
    });
  });
}

function strategyCbLogin(req, email, password, done) {
  User.findOne({ 'local.email': email }, (err, user) => {
    if (err) { return done(err); }
    if (!user) { return done(null, false, req.flash('loginMessage', 'No user found.')); }
    if (!user.validPassword(password)) {
      return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'));
    }
    return done(null, user);
  });
}

module.exports = (passport) => {
  passport.serializeUser((user, done) => { done(null, user.id); });

  passport.deserializeUser((id, done) => {
    User.findById(id, (err, user) => { done(err, user); });
  });

  const localStrategySignup = new LocalStrategy(strategySettings, strategyCbSignup);
  const localStrategyLogin = new LocalStrategy(strategySettings, strategyCbLogin);
  passport.use('local-signup', localStrategySignup);
  passport.use('local-login', localStrategyLogin);
};
